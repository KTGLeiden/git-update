﻿using System;
using System.Collections.Generic;
using System.IO;

namespace auto.update
{
    class Program
    {
        static void Main(string[] args)
        {
            var currentDir = System.Environment.CurrentDirectory;
            List<string> dirs = new List<string>(Directory.EnumerateDirectories(currentDir));
            GitOperations.CheckoutMaster();
            GitOperations.Pull();

            // Temp branch name
            string branchName = "test-branch";

            GitOperations.DeleteBranch(branchName);
            GitOperations.CheckoutNewBranch(branchName);
            // Do some stuff
            CreateRandomFile(Path.Combine(currentDir, "test"));
            // Add, commit, push changes and create a pull request
            GitOperations.AddAll();
            GitOperations.Commit("Description of changes. Can be anything");
            GitOperations.PushNew(branchName);
            // NOTE: THIS WILL NOT WORK!
            // READ https://gitlab.com/gitlab-org/gitlab-ce/issues/21054 FOR GITLAB
            // SEE https://developer.github.com/v3/pulls/ FOR GITHUB?
            // THIS BELOW JUSTS GENERATES AN OVERVIEW OF CHANGES.
            GitOperations.CreatePullRequest(branchName, "origin/master");
        }

        static void CreateRandomFile(string dir) {
            // Create dir
            Directory.CreateDirectory(dir);
            // Create a file name for the file we want to create.
            string fileName = Path.GetRandomFileName();
            string pathString = Path.Combine(dir, fileName);
            // Check that the file doesn't already exist. If it doesn't exist, create
            // the file and write integers 0 - 99 to it.
            // DANGER: System.IO.File.Create will overwrite the file if it already exists.
            // This could happen even with random file names, although it is unlikely.
            if (!System.IO.File.Exists(pathString))
            {
                using (System.IO.FileStream fs = System.IO.File.Create(pathString))
                {
                    for (byte i = 0; i < 100; i++)
                    {
                        fs.WriteByte(i);
                    }
                }
            }
            else
            {
                Console.WriteLine("File \"{0}\" already exists.", fileName);
                return;
            }

        }
    }
}
