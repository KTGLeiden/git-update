using System;
using System.Diagnostics;
public static class ShellHelper
{
    public static string Git(this string cmd)
    {
        var process = new Process()
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = "git",
                Arguments = cmd,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
            }
        };
        Console.WriteLine($"git {cmd}");
        process.Start();
        string result = process.StandardOutput.ReadToEnd();
        Console.WriteLine(result);
        process.WaitForExit();
        Console.WriteLine($"git {cmd} finished");
        return result;
    }
}