/** Class that includes github functionalities */
public class GitOperations {

    private static void ExecuteGitCommand(string gitCommand) {
        gitCommand.Git();
    }

    /// <summary>
    /// Checks out master
    /// </summary>
    public static void CheckoutMaster() {
        ExecuteGitCommand("checkout master");
    }

    /// <summary>
    /// Pulls the current branch
    /// </summary>
    public static void Pull() {
        ExecuteGitCommand("pull");
    }

    /// <summary>
    /// Checks out a new branch
    /// </summary>
    /// <param name="branchName"></param>
    public static void CheckoutNewBranch(string branchName) {
        ExecuteGitCommand($"checkout -b {branchName}");
    }

    /// <summary>
    /// Deletes branch
    /// </summary>
    /// <param name="branchName"></param>
    public static void DeleteBranch(string branchName) {
        ExecuteGitCommand($"branch -d {branchName}");
    }

    /// <summary>
    /// Creates PR  
    /// </summary>
    /// <param name="sourceBranch">Source branch from which to take changes from</param>
    /// <param name="targetBranch">Target branch to merge with</param>
    public static void CreatePullRequest(string sourceBranch, string targetBranch) {
        ExecuteGitCommand($"request-pull {targetBranch} {sourceBranch}");
    }

    /// <summary>
    /// Adds all changes
    /// </summary>
    public static void AddAll() {
        ExecuteGitCommand("add *");
    }

    /// <summary>
    /// Commits changes to current branch
    /// </summary>
    /// <param name="message"></param>
    public static void Commit(string message) {
        ExecuteGitCommand($"commit -m \"{message}\"");
    }

    /// <summary>
    /// Pushes a new branch to git
    /// </summary>
    public static void PushNew(string branchName) {
        ExecuteGitCommand($"push -u origin {branchName}");
    }
}